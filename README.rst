Sales Order
***********

Install
=======

Virtual Environment
-------------------

::

  virtualenv --python=python3 venv-sales-order
  # or
  python3 -m venv venv-sales-order

  source venv-sales-order/bin/activate

  pip install -r requirements/local.txt

Testing
=======

::

  find . -name '*.pyc' -delete
  pytest -x

Usage
=====

::

  ./init_dev.sh

Release
=======

https://www.kbsoftware.co.uk/docs/
