# -*- encoding: utf-8 -*-
from django.urls import re_path

from .views import (
    SalesOrderContactListView,
    SalesOrderCreateView,
    SalesOrderListView,
    SalesOrderUpdateView,
)


urlpatterns = [
    re_path(r"^$", view=SalesOrderListView.as_view(), name="sales.order.list"),
    re_path(
        r"^(?P<pk>\d+)/update/$",
        view=SalesOrderUpdateView.as_view(),
        name="sales.order.update",
    ),
    re_path(
        r"^contact/(?P<contact_pk>\d+)/$",
        view=SalesOrderContactListView.as_view(),
        name="sales.order.contact.list",
    ),
    re_path(
        r"^contact/(?P<contact_pk>\d+)/create/$",
        view=SalesOrderCreateView.as_view(),
        name="sales.order.create",
    ),
]
