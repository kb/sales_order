# -*- encoding: utf-8 -*-
import logging

from decimal import Decimal

from django.apps import apps
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from reversion import revisions as reversion

from base.model_utils import (
    TimedCreateModifyDeleteVersionModel,
    TimedCreateModifyDeleteVersionModelManager,
    TimeStampedModel,
)


logger = logging.getLogger(__name__)


def get_contact_model():
    return apps.get_model(settings.CONTACT_MODEL)


class SalesOrderError(Exception):
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value

    def __str__(self):
        return repr("%s, %s" % (self.__class__.__name__, self.value))


class SalesOrderTypeManager(models.Manager):
    def _create_sales_order_type(self, title, is_estimated):
        x = self.model(title=title, is_estimated=is_estimated)
        x.save()
        return x

    def init_sales_order_type(self, title, is_estimated):
        try:
            x = self.get(title=title)
            x.is_estimated = is_estimated
            x.save()
        except self.model.DoesNotExist:
            x = self._create_sales_order_type(title, is_estimated)
        return x


@reversion.register()
class SalesOrderType(TimeStampedModel):
    """Type of sales order.

    1. Do we even need a type?
    2. I think ``is_estimated`` is probably the wrong field name.
       The type of a sales order could be:

       - Agreed by email
       - Agreed by prior arrangement
       - Fixed Price
       - Support Contract

       So do we need ``is_estimated`` at all?

    """

    title = models.CharField(max_length=100)
    is_estimated = models.BooleanField(default=False)
    objects = SalesOrderTypeManager()

    class Meta:
        ordering = ("title",)
        verbose_name = "Sales Order Type"

    def __str__(self):
        return "{}".format(self.title)


class SalesOrderManager(TimedCreateModifyDeleteVersionModelManager):
    """Sales order model manager.

    You can find an ``_init_sales_order`` method in::

      kbsoftware_couk/project/management/commands/4473-create-sales-order.py

    """

    def current(self, contact=None):
        qs = self.model.objects.exclude(deleted=True)
        if contact is None:
            pass
        else:
            qs = qs.filter(contact=contact)
        return qs


@reversion.register()
class SalesOrder(TimedCreateModifyDeleteVersionModel):
    """Sales order.

    A sales order is created for a ``Contact`` (not for a ticket).

    ``sales_order_type``
      I am not sure what this is used for... Could be an estimate?
    ``hours``
      Number of hours agreed with the customer.
    ``hours_type``
      Determines if the ticket is fixed price (``FIXED_PRICE``) or not.
    ``value``
      Value of the work agreed with the customer.
    ``start_date`` and ``end_date``
      Period of time when this sales order is valid e.g. for yearly contracts.
    ``maximum_hours_per_ticket``
      Several of our customers let us do ``x`` hours of work on a ticket without
      checking first.  Work exceeding ``maximum_hours_per_ticket`` will need a
      separate sales order.
    ``completed``
      Has this sales order been completed?

    """

    UNIQUE_FIELD_NAME = "number"
    UNIQUE_FIELD_NAME_CATEGORY = None

    # HOURS_PER_MONTH = "hours-per-month"
    FIXED_PRICE = "fixed-price"
    HOURS_PER_MONTH = "hours-per-month"
    HOURS_PER_TICKET = "hours-per-ticket"
    HOURS_TOTAL = "hours-total"
    NO_LIMIT = "no-limit"
    SUPPORT_ANNUAL = "support-annual"
    SUPPORT_MONTHLY = "support-monthly"

    HOURS_TYPE = (
        (HOURS_TOTAL, "Total Hours"),
        (HOURS_PER_MONTH, "Hours per Month"),
        (HOURS_PER_TICKET, "Hours per Ticket"),
        (FIXED_PRICE, "Fixed Price (Value of Sales Order)"),
        (NO_LIMIT, "No Limit"),
        (SUPPORT_ANNUAL, "Annual Support / Maintenance"),
        (SUPPORT_MONTHLY, "Monthly Support / Maintenance"),
    )

    number = models.IntegerField(default=0)
    contact = models.ForeignKey(
        settings.CONTACT_MODEL, on_delete=models.CASCADE
    )
    title = models.CharField(max_length=100)
    sales_order_type = models.ForeignKey(
        SalesOrderType, on_delete=models.CASCADE
    )
    description = models.TextField(blank=True, null=True)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name="+", on_delete=models.CASCADE
    )
    # estimated = models.BooleanField()
    hours = models.IntegerField(
        blank=True, null=True, help_text="Hours agreed with client"
    )
    hours_type = models.SlugField(
        max_length=20,
        choices=HOURS_TYPE,
        default=HOURS_TOTAL,
        help_text="Hours limited by",
    )
    value = models.DecimalField(
        blank=True,
        null=True,
        max_digits=8,
        decimal_places=2,
        help_text="Value of sales order",
    )
    start_date = models.DateField(
        blank=True, null=True, help_text="Start date of support contract"
    )
    end_date = models.DateField(
        blank=True, null=True, help_text="End date of support contract"
    )
    # 29/12/2023, This field is also used for 'maximum_hours_per_month'
    maximum_hours_per_ticket = models.IntegerField(
        blank=True,
        null=True,
        help_text="No charge for work taking longer (per month / per ticket)",
    )
    completed = models.BooleanField(
        default=False, help_text="Complete date (if not a support contract)"
    )
    completed_date = models.DateTimeField(blank=True, null=True)
    completed_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        blank=True,
        null=True,
        related_name="+",
        on_delete=models.CASCADE,
    )
    objects = SalesOrderManager()

    class Meta:
        ordering = ("-number",)
        unique_together = ("number", "deleted_version")
        verbose_name = "Sales Order"
        verbose_name_plural = "Sales Orders"

    def __str__(self):
        return "{}: {} for {}".format(
            self.number, self.title, self.contact.full_name
        )

    def hourly_rate_for_contact(self):
        result = None
        try:
            invoice_contact = self.contact.invoicecontact
        except ObjectDoesNotExist:
            raise SalesOrderError(
                f"Contact ('{self.contact.user.username}') "
                "has no 'InvoiceContact' (for the 'hourly_rate') "
                f"(for sales order {self.number})"
            )
        if invoice_contact.hourly_rate:
            result = invoice_contact.hourly_rate
        else:
            raise SalesOrderError(
                f"Contact ('{self.contact.user.username}') has "
                f"no 'hourly_rate' (for sales order {self.number})"
            )
        return result

    def maximum_value(self):
        result = None
        if self.hours_type == self.HOURS_TOTAL:
            if self.hours and self.hours > Decimal():
                result = Decimal(self.hours) * self.hourly_rate_for_contact()
            else:
                raise SalesOrderError(
                    f"Sales order is '{self.hours_type}', but "
                    "'hours' has not been set"
                )
        return result

    def maximum_value_per_ticket(self):
        result = None
        if self.hours_type == self.HOURS_PER_TICKET:
            if (
                self.maximum_hours_per_ticket
                and self.maximum_hours_per_ticket > Decimal()
            ):
                result = (
                    Decimal(self.maximum_hours_per_ticket)
                    * self.hourly_rate_for_contact()
                )
            else:
                raise SalesOrderError(
                    f"Sales order is '{self.hours_type}', but the "
                    "'maximum_hours_per_ticket' has not been set"
                )
        return result

    @property
    def sales_order_number(self):
        return "{:06d}".format(self.number)

    def summary(self):
        """Summary of the sales order for 'time-record-pandas'.

        ::

          invoice/management/commands/time-record-pandas.py

        """
        result = {
            "contact": self.contact.user.username,
            "sales-order-number": self.sales_order_number,
            "title": self.title,
        }
        try:
            result.update({"hourly-rate": self.hourly_rate_for_contact()})
        except SalesOrderError:
            pass
        if self.hours_type:
            result.update({"hours-type": self.get_hours_type_display()})
        if self.value:
            result.update({"value": self.value})
        return result
