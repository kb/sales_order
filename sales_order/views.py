# -*- encoding: utf-8 -*-
from braces.views import LoginRequiredMixin, StaffuserRequiredMixin
from django.urls import reverse
from django.views.generic import CreateView, DetailView, ListView, UpdateView

from base.view_utils import BaseMixin
from .forms import SalesOrderForm
from .models import get_contact_model, SalesOrder


class SalesOrderContactListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    paginate_by = 20

    def _contact(self):
        pk = self.kwargs["contact_pk"]
        return get_contact_model().objects.get(pk=pk)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(contact=self._contact()))
        return context

    def get_queryset(self):
        contact = self._contact()
        return SalesOrder.objects.current(contact)


class SalesOrderCreateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, CreateView
):
    form_class = SalesOrderForm
    model = SalesOrder

    def _contact(self):
        pk = self.kwargs["contact_pk"]
        return get_contact_model().objects.get(pk=pk)

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.number = self.model.objects.next_number()
        self.object.completed = False
        self.object.contact = self._contact()
        self.object.user = self.request.user
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(contact=self._contact()))
        return context

    def get_success_url(self):
        return reverse(
            "sales.order.contact.list", args=[self.object.contact.pk]
        )


class SalesOrderListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    paginate_by = 20

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(contact=None))
        return context

    def get_queryset(self):
        return SalesOrder.objects.current()


class SalesOrderUpdateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    form_class = SalesOrderForm
    model = SalesOrder

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(contact=self.object.contact))
        return context

    def get_success_url(self):
        return reverse("sales.order.detail", args=[self.object.pk])
