# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from crm.models import Funded, KanbanLane
from sales_order.models import SalesOrderType


class Command(BaseCommand):
    help = "Initialise Sales Order app"

    def _funded(self):
        Funded.objects.init_funded(
            Funded.FREE_OF_CHARGE, "Free of Charge", True, False
        )
        Funded.objects.init_funded(
            Funded.SALES_ORDER, "Sales Order", False, True
        )

    def _kanban_lane(self, title):
        kanban_lane = KanbanLane.objects.get(title=title)
        kanban_lane.funded = True
        kanban_lane.save()

    def _sales_order_type(self):
        SalesOrderType.objects.init_sales_order_type("Agreed by email", False)
        SalesOrderType.objects.init_sales_order_type(
            "Agreed by prior arrangement", False
        )
        SalesOrderType.objects.init_sales_order_type("Estimated", True)
        SalesOrderType.objects.init_sales_order_type("Support Contract", False)
        SalesOrderType.objects.init_sales_order_type("Verbal", False)

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        self._kanban_lane("Development")
        self._kanban_lane("Support")
        self._funded()
        self._sales_order_type()
        self.stdout.write("{} - Complete".format(self.help))
