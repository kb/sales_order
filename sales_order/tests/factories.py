# -*- encoding: utf-8 -*-
import factory

from login.tests.factories import UserFactory
from sales_order.models import SalesOrder, SalesOrderType


class SalesOrderTypeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = SalesOrderType

    @factory.sequence
    def title(n):
        return "title_{:02d}".format(n)


class SalesOrderFactory(factory.django.DjangoModelFactory):
    user = factory.SubFactory(UserFactory)
    sales_order_type = factory.SubFactory(SalesOrderTypeFactory)

    class Meta:
        model = SalesOrder

    @factory.sequence
    def number(n):
        return n + 1

    @factory.sequence
    def title(n):
        return "title_{:02d}".format(n)
