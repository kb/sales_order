# -*- encoding: utf-8 -*-
import pytest

from django.core.management import call_command

from crm.tests.factories import KanbanLaneFactory


@pytest.mark.django_db
def test_init_app_sales_order():
    KanbanLaneFactory(title="Development")
    KanbanLaneFactory(title="Support")
    call_command("init-app-sales-order")
