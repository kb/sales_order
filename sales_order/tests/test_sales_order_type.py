# -*- encoding: utf-8 -*-
import pytest

from sales_order.models import SalesOrderType
from sales_order.tests.factories import SalesOrderTypeFactory


@pytest.mark.django_db
def test_init_sales_order():
    x = SalesOrderType.objects.init_sales_order_type("Fixed Price", False)
    assert "Fixed Price" == x.title
    assert x.is_estimated is False


@pytest.mark.django_db
def test_ordering():
    SalesOrderTypeFactory(title="C")
    SalesOrderTypeFactory(title="B")
    SalesOrderTypeFactory(title="A")
    assert ["A", "B", "C"] == [x.title for x in SalesOrderType.objects.all()]


@pytest.mark.django_db
def test_str():
    x = SalesOrderTypeFactory(title="Estimate")
    assert "Estimate" == str(x)
