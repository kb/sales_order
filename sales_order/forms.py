# -*- encoding: utf-8 -*-
from django import forms

from base.form_utils import RequiredFieldForm
from .models import SalesOrder


class SalesOrderEmptyForm(forms.ModelForm):
    class Meta:
        model = SalesOrder
        fields = ()


class SalesOrderForm(RequiredFieldForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["title"].widget.attrs.update({"class": "pure-input-1"})
        self.fields["description"].widget.attrs.update(
            {"class": "pure-input-1", "rows": 5}
        )
        for name in ("hours_type", "sales_order_type"):
            self.fields[name].widget.attrs.update({"class": "chosen-select"})
        self.fields["maximum_hours_per_ticket"].label = (
            "Maximum hours per month / per ticket"
        )

    class Meta:
        model = SalesOrder
        fields = [
            "title",
            "sales_order_type",
            "description",
            # "estimated",
            "hours",
            "hours_type",
            "value",
            "start_date",
            "end_date",
            "maximum_hours_per_ticket",
        ]

    def clean(self):
        cleaned_data = super().clean()
        hours = cleaned_data.get("hours")
        maximum_hours = cleaned_data.get("maximum_hours_per_ticket")
        value = cleaned_data.get("value")
        start_date = cleaned_data.get("start_date")
        end_date = cleaned_data.get("end_date")
        if (
            (hours and maximum_hours)
            or (maximum_hours and value)
            or (hours and value)
        ):
            raise forms.ValidationError(
                "A sales order can be for hours, a value or maximum hours",
                code="sales_order__hours_or_value_or_maximum",
            )
        if start_date or end_date:
            if start_date and end_date:
                if start_date > end_date:
                    raise forms.ValidationError(
                        "The start date must be before the end date",
                        code="sales_order__start_before_end",
                    )
            else:
                raise forms.ValidationError(
                    "If a sales order has a start date or "
                    "end date, it must have both",
                    code="sales_order__start_and_end_date",
                )
        return cleaned_data
