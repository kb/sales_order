# -*- encoding: utf-8 -*-
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import include, path, re_path

from .views import (
    ContactDetailView,
    DashView,
    HomeView,
    SalesOrderDetailView,
    SettingsView,
)


urlpatterns = [
    re_path(r"^", view=include("login.urls")),
    re_path(r"^$", view=HomeView.as_view(), name="project.home"),
    re_path(r"^chat/", view=include("chat.urls")),
    re_path(r"^contact/", view=include("contact.urls")),
    re_path(
        r"^contact/(?P<pk>\d+)/$",
        view=ContactDetailView.as_view(),
        name="contact.detail",
    ),
    re_path(r"^crm/", view=include("crm.urls")),
    re_path(r"^dash/$", view=DashView.as_view(), name="project.dash"),
    # re_path(r"^finance/", view=include("finance.urls")),
    re_path(r"^invoice/", view=include("invoice.urls")),
    re_path(r"^mail/", view=include("mail.urls")),
    re_path(r"^report/", view=include("report.urls")),
    re_path(r"^sales_order/", view=include("sales_order.urls")),
    re_path(
        r"^sales_order/(?P<pk>\d+)/$",
        view=SalesOrderDetailView.as_view(),
        name="sales.order.detail",
    ),
    re_path(
        r"^settings/$",
        view=SettingsView.as_view(),
        name="project.settings",
    ),
    re_path(r"^stock/", view=include("stock.urls")),
]

urlpatterns += staticfiles_urlpatterns()

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        re_path(r"^__debug__/", include(debug_toolbar.urls))
    ] + urlpatterns
