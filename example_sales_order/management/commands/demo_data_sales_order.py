# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from chat.models import Channel
from crm.models import Priority
from sales_order.models import SalesOrderType


class Command(BaseCommand):
    help = "Sales Order Demo Data"

    def _init_channel(self, slug):
        Channel.objects.init_channel("activity")

    def _init_priority(self, name, level):
        """Nothing clever here... Just get the levels created."""
        try:
            Priority.objects.get(level=level)
        except Priority.DoesNotExist:
            Priority(name=name, level=level).save()

    def handle(self, *args, **options):
        self.stdout.write(self.help)
        self._init_channel("activity")
        self._init_priority("High", 3)
        self._init_priority("Medium", 2)
        self._init_priority("Low", 1)
        self.stdout.write("Complete: {}".format(self.help))
