# -*- encoding: utf-8 -*-
from braces.views import LoginRequiredMixin, StaffuserRequiredMixin
from django.views.generic import DetailView, TemplateView

from base.view_utils import BaseMixin
from crm.models import Ticket
from contact.models import Contact
from contact.views import ContactDetailMixin
from sales_order.models import SalesOrder


class ContactDetailView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    ContactDetailMixin,
    BaseMixin,
    DetailView,
):
    template_name = "example/contact_detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(
            dict(
                salesorder_list=SalesOrder.objects.current(self.object),
                ticket_list=Ticket.objects.current(self.object),
            )
        )
        return context


class DashView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, TemplateView
):
    template_name = "example/dash.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(contact_list=Contact.objects.current()))
        return context


class HomeView(BaseMixin, TemplateView):
    template_name = "example/home.html"


class SalesOrderDetailView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, DetailView
):
    model = SalesOrder
    template_name = "example/sales_order_detail.html"


class SettingsView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, TemplateView
):
    template_name = "example/settings.html"
