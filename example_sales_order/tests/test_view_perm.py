# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse

from contact.tests.factories import ContactFactory, UserContactFactory
from login.tests.fixture import perm_check
from sales_order.tests.factories import SalesOrderFactory


@pytest.mark.django_db
def test_contact_detail(perm_check):
    user_contact = UserContactFactory()
    url = reverse("contact.detail", args=[user_contact.contact.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_dash(perm_check):
    perm_check.staff(reverse("project.dash"))


@pytest.mark.django_db
def test_home(perm_check):
    perm_check.anon(reverse("project.home"))


@pytest.mark.django_db
def test_sales_order_contact_list(perm_check):
    contact = ContactFactory()
    perm_check.staff(reverse("sales.order.contact.list", args=[contact.pk]))


@pytest.mark.django_db
def test_sales_order_create(perm_check):
    contact = ContactFactory()
    perm_check.staff(reverse("sales.order.create", args=[contact.pk]))


@pytest.mark.django_db
def test_sales_order_detail(perm_check):
    sales_order = SalesOrderFactory(contact=ContactFactory())
    perm_check.staff(reverse("sales.order.detail", args=[sales_order.pk]))


@pytest.mark.django_db
def test_sales_order_list(perm_check):
    perm_check.staff(reverse("sales.order.list"))


@pytest.mark.django_db
def test_sales_order_update(perm_check):
    sales_order = SalesOrderFactory(contact=ContactFactory())
    perm_check.staff(reverse("sales.order.update", args=[sales_order.pk]))


@pytest.mark.django_db
def test_settings(perm_check):
    perm_check.staff(reverse("project.settings"))
