# -*- encoding: utf-8 -*-
import pytest

from datetime import date
from decimal import Decimal
from django.urls import reverse
from http import HTTPStatus

from contact.tests.factories import ContactFactory
from login.tests.factories import TEST_PASSWORD, UserFactory
from sales_order.models import SalesOrder
from sales_order.tests.factories import SalesOrderFactory, SalesOrderTypeFactory


@pytest.mark.parametrize(
    "hours,value,maximum_hours,hours_expect,value_expect,maximum_hours_expect",
    [
        ("", Decimal("600"), "", None, Decimal("600"), None),
        (21, "", "", 21, None, None),
        ("", "", 4, None, None, 4),
    ],
)
@pytest.mark.django_db
def test_sales_order_create(
    client,
    hours,
    value,
    maximum_hours,
    hours_expect,
    value_expect,
    maximum_hours_expect,
):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    contact = ContactFactory(company_name="KB")
    sales_order_type = SalesOrderTypeFactory()
    assert 0 == SalesOrder.objects.count()
    response = client.post(
        reverse("sales.order.create", args=[contact.pk]),
        {
            "title": "Carrot",
            "description": "Vegetable",
            "sales_order_type": sales_order_type.pk,
            "hours": hours,
            "hours_type": SalesOrder.HOURS_TOTAL,
            "value": value,
            "start_date": date(2018, 1, 21),
            "end_date": date(2019, 1, 20),
            "maximum_hours_per_ticket": maximum_hours,
        },
    )
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert (
        reverse("sales.order.contact.list", args=[contact.pk]) == response.url
    )
    assert 1 == SalesOrder.objects.count()
    sales_order = SalesOrder.objects.first()
    assert contact == sales_order.contact
    assert 1 == sales_order.number
    assert sales_order.completed is False
    assert "Carrot" == sales_order.title
    assert sales_order_type.pk == sales_order.sales_order_type.pk
    assert "Vegetable" == sales_order.description
    assert hours_expect == sales_order.hours
    assert value_expect == sales_order.value
    assert date(2018, 1, 21) == sales_order.start_date
    assert date(2019, 1, 20) == sales_order.end_date
    assert maximum_hours_expect == sales_order.maximum_hours_per_ticket
    assert user == sales_order.user


@pytest.mark.parametrize(
    "hours,value,maximum_hours_per_ticket",
    [
        (22, Decimal("600"), 4),
        (None, Decimal("600"), 4),
        (22, None, 4),
        (22, Decimal("600"), None),
    ],
)
@pytest.mark.django_db
def test_sales_order_create_hours_or_value_or_maximum_hours(
    client, hours, value, maximum_hours_per_ticket
):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    data = {
        "title": "Carrot",
        "sales_order_type": SalesOrderTypeFactory().pk,
        "start_date": date(2018, 1, 21),
        "end_date": date(2019, 1, 20),
        "hours_type": SalesOrder.HOURS_TOTAL,
    }
    if hours:
        data.update({"hours": hours})
    if maximum_hours_per_ticket:
        data.update({"maximum_hours_per_ticket": maximum_hours_per_ticket})
    if value:
        data.update({"value": value})
    response = client.post(
        reverse("sales.order.create", args=[ContactFactory().pk]), data
    )
    assert HTTPStatus.OK == response.status_code
    assert {
        "__all__": ["A sales order can be for hours, a value or maximum hours"]
    } == response.context["form"].errors


@pytest.mark.django_db
def test_sales_order_create_no_title(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.post(
        reverse("sales.order.create", args=[ContactFactory().pk]),
        {
            "sales_order_type": SalesOrderTypeFactory().pk,
            "value": Decimal("600"),
            "start_date": date(2018, 1, 21),
            "end_date": date(2019, 1, 20),
            "hours_type": SalesOrder.HOURS_TOTAL,
        },
    )
    assert HTTPStatus.OK == response.status_code
    assert {"title": ["This field is required."]} == response.context[
        "form"
    ].errors


@pytest.mark.django_db
def test_sales_order_create_start_date_and_end_date(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.post(
        reverse("sales.order.create", args=[ContactFactory().pk]),
        {
            "title": "Carrot",
            "sales_order_type": SalesOrderTypeFactory().pk,
            "hours": 21,
            "hours_type": SalesOrder.HOURS_TOTAL,
            "end_date": date(2019, 1, 20),
        },
    )
    assert HTTPStatus.OK == response.status_code
    assert {
        "__all__": [
            "If a sales order has a start date or end date, it must have both"
        ]
    } == response.context["form"].errors


@pytest.mark.django_db
def test_sales_order_create_start_date_before_end_date(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.post(
        reverse("sales.order.create", args=[ContactFactory().pk]),
        {
            "title": "Carrot",
            "sales_order_type": SalesOrderTypeFactory().pk,
            "hours": 21,
            "hours_type": SalesOrder.HOURS_TOTAL,
            "start_date": date(2019, 3, 21),
            "end_date": date(2019, 1, 20),
        },
    )
    assert HTTPStatus.OK == response.status_code
    assert {
        "__all__": ["The start date must be before the end date"]
    } == response.context["form"].errors


@pytest.mark.django_db
def test_sales_order_list(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    SalesOrderFactory(contact=ContactFactory(company_name="KB"))
    response = client.get(reverse("sales.order.list"))
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_sales_order_update(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    contact = ContactFactory(company_name="KB")
    sales_order_type = SalesOrderTypeFactory()
    sales_order = SalesOrderFactory(
        contact=contact,
        title="Orange",
        sales_order_type=SalesOrderTypeFactory(),
        description="Fruit",
        hours=5,
        value=Decimal("500"),
        start_date=date(2018, 1, 21),
        end_date=date(2019, 1, 20),
        maximum_hours_per_ticket=4,
    )
    response = client.post(
        reverse("sales.order.update", args=[sales_order.pk]),
        {
            "title": "Carrot",
            "sales_order_type": sales_order_type.pk,
            "description": "Vegetable",
            "hours": 21,
            "hours_type": SalesOrder.HOURS_TOTAL,
            "start_date": date(2019, 3, 21),
            "end_date": date(2020, 3, 20),
        },
    )
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert reverse("sales.order.detail", args=[sales_order.pk]) == response.url
    sales_order.refresh_from_db()
    assert "Carrot" == sales_order.title
    assert sales_order_type.pk == sales_order.sales_order_type.pk
    assert "Vegetable" == sales_order.description
    assert 21 == sales_order.hours
    assert sales_order.value is None
    assert date(2019, 3, 21) == sales_order.start_date
    assert date(2020, 3, 20) == sales_order.end_date
    assert sales_order.maximum_hours_per_ticket is None
