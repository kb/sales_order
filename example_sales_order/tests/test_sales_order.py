# -*- encoding: utf-8 -*-
import pytest

from decimal import Decimal

from contact.tests.factories import ContactFactory
from invoice.tests.factories import InvoiceContactFactory
from login.tests.factories import UserFactory
from sales_order.models import SalesOrder, SalesOrderError
from sales_order.tests.factories import SalesOrderFactory


@pytest.mark.django_db
def test_current():
    SalesOrderFactory(number=1, title="A", contact=ContactFactory())
    sales_order = SalesOrderFactory(
        number=2, title="B", contact=ContactFactory()
    )
    SalesOrder.objects.set_deleted(sales_order, UserFactory())
    SalesOrderFactory(number=3, title="C", contact=ContactFactory())
    assert ["C", "A"] == [x.title for x in SalesOrder.objects.current()]


@pytest.mark.django_db
def test_current_contact():
    contact = ContactFactory()
    SalesOrderFactory(number=1, title="A", contact=contact)
    sales_order = SalesOrderFactory(number=2, title="B", contact=contact)
    SalesOrder.objects.set_deleted(sales_order, UserFactory())
    SalesOrderFactory(number=3, title="C", contact=ContactFactory())
    SalesOrderFactory(number=4, title="D", contact=contact)
    assert ["D", "A"] == [x.title for x in SalesOrder.objects.current(contact)]


@pytest.mark.django_db
def test_hourly_rate_for_contact():
    contact = ContactFactory()
    InvoiceContactFactory(contact=contact, hourly_rate=Decimal("20"))
    sales_order = SalesOrderFactory(contact=contact)
    assert Decimal("20") == sales_order.hourly_rate_for_contact()


@pytest.mark.django_db
def test_hourly_rate_for_contact_no_hourly_rate():
    contact = ContactFactory(user=UserFactory(username="patrick"))
    InvoiceContactFactory(contact=contact, hourly_rate=None)
    sales_order = SalesOrderFactory(contact=contact, number=231)
    with pytest.raises(SalesOrderError) as e:
        sales_order.hourly_rate_for_contact()
    assert (
        "Contact ('patrick') has no 'hourly_rate' (for sales order 231)"
        in str(e.value)
    )


@pytest.mark.django_db
def test_hourly_rate_for_contact_no_invoice_contact():
    contact = ContactFactory(user=UserFactory(username="patrick"))
    # InvoiceContactFactory(contact=contact, hourly_rate=Decimal("20"))
    sales_order = SalesOrderFactory(contact=contact, number=231)
    with pytest.raises(SalesOrderError) as e:
        sales_order.hourly_rate_for_contact()
    assert (
        "Contact ('patrick') has no 'InvoiceContact' "
        "(for the 'hourly_rate') (for sales order 231)" in str(e.value)
    )


@pytest.mark.django_db
def test_maximum_value():
    contact = ContactFactory()
    InvoiceContactFactory(contact=contact, hourly_rate=Decimal("20"))
    sales_order = SalesOrderFactory(contact=contact, hours=Decimal("100"))
    assert Decimal("2000") == sales_order.maximum_value()


@pytest.mark.django_db
def test_maximum_value_no_hours():
    contact = ContactFactory()
    InvoiceContactFactory(contact=contact, hourly_rate=Decimal("20"))
    sales_order = SalesOrderFactory(contact=contact, hours=None)
    with pytest.raises(SalesOrderError) as e:
        sales_order.maximum_value()
    assert (
        "Sales order is 'hours-total', but 'hours' has not been set"
    ) in str(e.value)


@pytest.mark.django_db
def test_maximum_value_per_ticket():
    contact = ContactFactory()
    InvoiceContactFactory(contact=contact)
    sales_order = SalesOrderFactory(
        hours_type=SalesOrder.HOURS_PER_TICKET,
        contact=contact,
        maximum_hours_per_ticket=Decimal("10"),
    )
    assert Decimal("200") == sales_order.maximum_value_per_ticket()


@pytest.mark.django_db
def test_maximum_value_per_ticket_no_maximum_hours_per_ticket():
    contact = ContactFactory()
    InvoiceContactFactory(contact=contact)
    sales_order = SalesOrderFactory(
        hours_type=SalesOrder.HOURS_PER_TICKET,
        contact=contact,
        maximum_hours_per_ticket=None,
    )
    with pytest.raises(SalesOrderError) as e:
        sales_order.maximum_value_per_ticket()
    assert (
        "Sales order is 'hours-per-ticket', but the "
        "'maximum_hours_per_ticket' has not been set"
    ) in str(e.value)


@pytest.mark.django_db
def test_maximum_value_per_ticket_not_hours_per_ticket():
    contact = ContactFactory()
    InvoiceContactFactory(contact=contact)
    sales_order = SalesOrderFactory(
        hours_type=SalesOrder.HOURS_TOTAL,
        contact=contact,
        maximum_hours_per_ticket=Decimal("100"),
    )
    assert sales_order.maximum_value_per_ticket() is None


@pytest.mark.django_db
def test_number():
    x = SalesOrderFactory(
        number=76, title="Apple", contact=ContactFactory(company_name="KB")
    )
    assert "000076" == x.sales_order_number


@pytest.mark.django_db
def test_str():
    contact = ContactFactory(company_name="KB")
    assert "76: Apple for KB" == str(
        SalesOrderFactory(number=76, title="Apple", contact=contact)
    )
